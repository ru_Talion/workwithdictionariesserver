The project accepts commands as command line arguments and in the console window.

- -help - show help
- -c - create a dictionary e. g. -с C:\\file.txt
- -u - update dictionary e. g. -u C:\\file.txt
- -d - delete dictionary
- -ex - exit
- -scs - set connection string e. g. -scs "Data Source=***lover=False"

      
