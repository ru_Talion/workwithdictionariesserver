﻿
namespace Domain.Entity
{
    public class Word
    {
        public Guid Id { get; set; }

        public required string Value { get; set; }
        public int NumberRepetitions { get; set; } = 1;
    }
}
