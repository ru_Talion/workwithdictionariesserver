﻿using Application.Abstract;
using Domain.Entity;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class WordsRepository : IWordsRepository
    {
        private readonly IDbContextFactory<MyDbContext> _dbContextFactory;

        public WordsRepository(IDbContextFactory<MyDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }
        public async Task AddWords(List<Word> words)
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();
            await dbContext.Words.AddRangeAsync(words);
            await dbContext.SaveChangesAsync();
        }

        public async Task UpdateWords(List<Word> words)
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();
            dbContext.Words.UpdateRange(words);
            await dbContext.SaveChangesAsync();
        }
        public async Task DeleteAllWords()
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();
            dbContext.Words.RemoveRange(dbContext.Words);
            await dbContext.SaveChangesAsync();
        }
        public async Task<List<Word>> GetAllWords()
        {
            using var dbContext = await _dbContextFactory.CreateDbContextAsync();
            return dbContext.Words.ToList();    
        }
    }
}
