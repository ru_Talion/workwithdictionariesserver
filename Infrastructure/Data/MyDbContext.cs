﻿
using Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Serilog;

namespace Infrastructure.Data
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options) 
        {
           
        }
        
        public DbSet<Word> Words { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Log.Error, LogLevel.Error);
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Word>()
                .HasIndex(u => u.Value)
                .IsUnique();
        }
    }
}
