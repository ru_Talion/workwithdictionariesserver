﻿using Domain.Entity;
using System.Text.RegularExpressions;

namespace Application.Service
{
    public class FromFileToListWordsConverter
    {
        public async Task<List<Word>> Convert(string pathToFile)
        {
            var test2 = pathToFile.Replace("\\", "/");
            var test3 = Regex.Unescape(test2).Trim('"').Trim('\\');

            if (!File.Exists(Path.GetFullPath(test3))) { throw new Exception("Invalid file path."); }

            using StreamReader sr = new StreamReader(Path.GetFullPath(test3));
            var line = await sr.ReadToEndAsync(CancellationToken.None);

            var inputWords = Regex.Replace(line, @"[^a-zA-Z]+", " ")
                .ToLower()
                .Split(" ")
                .Where(x => x.Length > 3 & x.Length <= 15)
                .GroupBy(p => p)
                .Select(g => new Word() { Value = g.Key, NumberRepetitions = g.Count() })
                .Where(x => x.NumberRepetitions > 3)
                .ToList();

            return inputWords;
        }
    }
}
