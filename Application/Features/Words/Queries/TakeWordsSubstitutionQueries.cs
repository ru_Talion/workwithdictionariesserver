﻿using Application.Abstract;
using Domain.Entity;
using F23.StringSimilarity;
using MediatR;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Application.Features.Words.Queries
{
    public class TakeWordsSubstitutionQueries : IRequest<List<Word>>
    {
        public required string InputFragment { get; set; }

        public class TakeWordsSubstitutionQueriesHandler : IRequestHandler<TakeWordsSubstitutionQueries, List<Word>>
        {
            private readonly IWordsRepository _wordsRepositories;

            public TakeWordsSubstitutionQueriesHandler(IWordsRepository wordsRepositories)
            {
                _wordsRepositories = wordsRepositories;
            }

            public async Task<List<Word>> Handle(TakeWordsSubstitutionQueries request, CancellationToken cancellationToken)
            {
                if(!Regex.IsMatch(request.InputFragment, "[a-zA-Z]")) { throw new ValidationException("Valid characters are A-Z and a-z."); };
                
                var wordsInDictionary = await _wordsRepositories.GetAllWords();

                var list = new List<(Word, double)> ();   
                
                var l = new Levenshtein();

                foreach (var word in wordsInDictionary)
                {
                    var partWord = word.Value.Substring(0, Math.Min(word.Value.Length, request.InputFragment.Length));

                    var distance = l.Distance(request.InputFragment.ToLower(), partWord);

                    list.Add((word, distance)); 
                }

                var result = list
                    .OrderBy(x => x.Item2)
                    .ThenByDescending(y => y.Item1.NumberRepetitions)
                    .ThenBy(z => z.Item1.Value)
                    .Take(5).Select(i => i.Item1).ToList();

                return result;
            }
        }

    }
}
