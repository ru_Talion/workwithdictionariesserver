﻿using Application.Abstract;
using Application.Service;
using Domain.Entity;
using MediatR;

namespace Application.Features.Words.Commands
{
    public class UpdateWords : IRequest<Unit>
    {
        public required string PathToFile { get; set; }

        public class AddWordsHandler : IRequestHandler<UpdateWords, Unit>
        {
            private readonly IWordsRepository _wordsRepositories;

            public AddWordsHandler(IWordsRepository wordsRepositories)
            {
                _wordsRepositories = wordsRepositories;
            }

            public async Task<Unit> Handle(UpdateWords request, CancellationToken cancellationToken)
            {
                var inputWords = await new FromFileToListWordsConverter().Convert(request.PathToFile);

                var wordsFromDb = await _wordsRepositories.GetAllWords();

                if (wordsFromDb.Count == 0)
                {
                    await _wordsRepositories.AddWords(inputWords);
                    return Unit.Value;
                }
                else
                {
                    var wordFusion = wordsFromDb.Concat(inputWords)
                        .GroupBy(p => p.Value)
                        .Select(g => new Word() { Value = g.Key, NumberRepetitions = g.Sum(x => x.NumberRepetitions), Id = g.FirstOrDefault(i => i.Id != null).Id })
                        .ToList();

                    await _wordsRepositories.UpdateWords(wordFusion);

                    return Unit.Value;
                }
            }
        }
    }
}
