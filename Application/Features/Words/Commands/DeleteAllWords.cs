﻿using Application.Abstract;
using MediatR;

namespace Application.Features.Words.Commands
{
    public class DeleteAllWords : IRequest<Unit>
    {
        public class DeleteAllWordsHandler : IRequestHandler<DeleteAllWords, Unit>
        {
            private readonly IWordsRepository _wordsRepositories;

            public DeleteAllWordsHandler(IWordsRepository wordsRepositories)
            {
                _wordsRepositories = wordsRepositories;
            }

            public async Task<Unit> Handle(DeleteAllWords request, CancellationToken cancellationToken)
            {
                await _wordsRepositories.DeleteAllWords();
                return Unit.Value;
            }
        }

    }
}
