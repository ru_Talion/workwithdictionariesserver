﻿using Domain.Entity;

namespace Application.Abstract
{
    public interface IWordsRepository
    {
        Task AddWords(List<Word> words);
        Task UpdateWords(List<Word> words);
        Task DeleteAllWords();
        Task<List<Word>> GetAllWords();
    }
}