using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Serilog;
using Web.Extensions;
using Web.Abstract;

internal class Program
{
    private static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.File("logs/myapp.txt", rollingInterval: RollingInterval.Day, retainedFileCountLimit: 2)
                .CreateLogger();

        builder.Services.RegisterServices();

        builder.Services.AddMediatR(cfg => {
            cfg.RegisterServicesFromAssemblies(AppDomain.CurrentDomain.GetAssemblies());
        });

        builder.Services.AddDbContextFactory<MyDbContext>(
            options => options.UseSqlServer(builder.Configuration.GetConnectionString("fromDB")));

        builder.Services.RegisterOptions(builder);

        var app = builder.Build();

        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseAuthorization();

        app.MapControllers();

        using IServiceScope scope = builder.Services.BuildServiceProvider().CreateScope();
        var consoleClient = scope.ServiceProvider.GetService<IConsoleApplication>();
        Task.Run(() => consoleClient!.Start(args));
        
        app.Run();
    }
}