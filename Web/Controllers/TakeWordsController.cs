using Application.Features.Words.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TakeWordsController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TakeWordsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] string userInput)
        {
            try
            {
                var words = await _mediator.Send(new TakeWordsSubstitutionQueries() { InputFragment = userInput });
                if (words.Count == 0) { return NoContent(); }
                return Ok(string.Join("\n", words.Select(x => x.Value).ToArray()));
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return BadRequest(ex.Message);
            }
        }
    }
}