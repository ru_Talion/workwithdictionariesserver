﻿namespace Web.Abstract
{
    abstract public class AppOptions
    {
        abstract public Dictionary<string, string> GetMainOptions();
    };
}
