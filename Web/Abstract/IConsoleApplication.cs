﻿namespace Web.Abstract
{
    public interface IConsoleApplication
    {
        void Start(string[] args);
    }
}