﻿using Web.Abstract;

namespace Web.Options
{
    public class ConnectionStrings : AppOptions
    {
        public string FromDB { get; set; } = string.Empty;

        override public Dictionary<string, string> GetMainOptions()
        {
            var options = new Dictionary<string, string>
            {
                { nameof(FromDB), FromDB },
            };
            return options;
        }

    }
}
