﻿using Web.Options;

namespace Web.Extensions
{
    public static class OptionsRegistrationExtensions
    {
        public static IServiceCollection RegisterOptions(this IServiceCollection services, WebApplicationBuilder builder)
        {
            services.Configure<ConnectionStrings>(builder.Configuration.GetSection(nameof(ConnectionStrings)));

            return services;
        }
    }
}
