﻿using Application.Abstract;
using Infrastructure.Repositories;
using Web.Abstract;

namespace Web.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {

            services.AddControllers();
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen();

            services.AddSingleton<IConsoleApplication, ConsoleApplication.ConsoleApplication>();
            services.AddScoped<IWordsRepository, WordsRepository>();

            return services;
        }
    }
}
