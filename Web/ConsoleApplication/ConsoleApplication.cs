﻿using Application.Features.Words.Commands;
using Application.Features.Words.Queries;
using Infrastructure.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using Web.Abstract;
using Web.Configuration;
using Web.Options;

namespace Web.ConsoleApplication
{
    public class ConsoleApplication : IConsoleApplication
    {
        private readonly IMediator _mediator;
        private readonly IDbContextFactory<MyDbContext> _dbContextFactory;

        public ConsoleApplication(IMediator mediator, IDbContextFactory<MyDbContext> dbContextFactory)
        {
            _mediator = mediator;
            _dbContextFactory = dbContextFactory;
        }

        public async void Start(string[] args)
        {
            if (args.Length > 2) { throw new ValidationException("More than two arguments are not allowed."); }

            while (true)
            {
                for (int i = 0; i < args.Length; i = 2)
                {
                    try
                    {
                        switch (args[i])
                        {
                            case "-help":
                                ShowHelp();
                                break;

                            case "-c":
                                await _mediator.Send(new DeleteAllWords());
                                await _mediator.Send(new UpdateWords() { PathToFile = args[i + 1]! });
                                break;

                            case "-u":

                                await _mediator.Send(new UpdateWords() { PathToFile = args[i + 1]! });
                                break;

                            case "-d":
                                await _mediator.Send(new DeleteAllWords());
                                break;


                            case "-scs":

                                var saveSett = new SaveToAppsettings();
                                var cS = new ConnectionStrings() { FromDB = Regex.Unescape(args[i + 1]!) };

                                saveSett.Save(cS);

                                using (var dbConext = await _dbContextFactory.CreateDbContextAsync())
                                {
                                    dbConext.Database.SetConnectionString(cS.FromDB);
                                    dbConext.Database.Migrate();
                                }
                                Console.WriteLine("After installing the connection string, the program needs to be restarted.");

                                break;

                            case "-ex":
                                Environment.Exit(0);
                                break;

                            default:
                                var words = await _mediator.Send(new TakeWordsSubstitutionQueries() { InputFragment = args[i] });
                                Console.WriteLine(string.Join("\n", words.Select(x => x.Value).ToArray()));
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }

                args = await Task.Run(() => Console.ReadLine()?.Split(' ', 2) ?? new string[] { "-ex" });

            }
        }
        public static void ShowHelp()
        {
            Console.WriteLine("" +
                "----Commands----\n" +
                "-help - show help\n" +
                "-c - create a dictionary e. g. -с C:\\file.txt\n" +
                "-u - update dictionary e. g. -u C:\\file.txt\n" +
                "-d - delete dictionary\n" +
                "-ex - exit\n" +
                "-scs - set connection string e. g. -scs \"Data Source=***lover=False\"");
        }
    }
}
