﻿using Newtonsoft.Json;
using System.Configuration;
using Web.Abstract;

namespace Web.Configuration
{
    public class SaveToAppsettings
    {
        public void Save(AppOptions mainOptions)
        {
            try
            {
                var filePath = Path.Combine(AppContext.BaseDirectory, "appsettings.Production.json");
                string json = File.ReadAllText(filePath);
                dynamic jsonObj = JsonConvert.DeserializeObject(json)!;

                var dicOption = mainOptions.GetMainOptions().Where(x => !string.IsNullOrWhiteSpace(x.Value));

                foreach (var ipt in dicOption)
                {
                    jsonObj[mainOptions.GetType().Name][ipt.Key] = ipt.Value;
                }

                string output = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
                File.WriteAllText(filePath, output);

            }
            catch (ConfigurationErrorsException ex)
            {
                throw new ConfigurationErrorsException(ex.Message);
            }
        }
    }
}
